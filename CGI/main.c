/* Show the CGI (Common Gateway Interface) environment variables */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define MAXLEN 80
#define EXTRA 5
#define MAXINPUT MAXLEN + EXTRA + 2

/* Print a basic HTTP header. */
static void print_http_header (const char * content_type)
{
    printf ("Content-Type: %s\n\n", content_type);
}

void unencode(char *src, char *last, char *dest)
{
    
    for (; src != last; src++, dest++)
    
        if (*src == '+')
            *dest = ' ';
        else if (*src == '%'){
            int code;
            if (sscanf(src + 1, "%2x", &code) != 1)
                code = '?';
            *dest = code;
            src += 2;
        }
        /*else if (*src == '&'){
            *dest = '\t';
            src +=5;
        }*/
        else
            *dest = *src;

    *dest;
    *++dest;
    
}

int main ()
{
    char *lenstr;
    char input[MAXINPUT], data[MAXINPUT];
    long len;

    print_http_header("text/html");

    printf("<HTML>\n");
    printf("<HEAD>\n");
    printf("<TITLE>Embedded Opdracht 2</TITLE>\n");
    printf("</HEAD>\n");
    printf("<BODY>\n");

    lenstr = getenv("CONTENT_LENGTH");

    printf( "%s", lenstr);

    if (lenstr == NULL || sscanf(lenstr, "%ld", &len) != 1 || len > MAXLEN)
    {
        printf("<P>Error in invocation - wrong FORM probably.");
    }
    else
    {
        FILE *datafile;
        fgets(input, len + 1, stdin);

        unencode(input + EXTRA, input + *lenstr, data);
        char *data_splt = strtok(data, "&");
        char test[4][40] = {};
        int i = 0;
        while (data_splt != NULL)
        {
            strcat(test[i], data_splt);
            i++;
            data_splt = strtok(NULL, "&");
        }
        char postedData[4][40] = {};
        int a = 1;
        for (int o = 0; o < 4; o++)
        {
            char *data_splt2 = strtok(test[o], "=");
            while (data_splt2 != NULL)
            {
                strcat(postedData[a], data_splt2);
                a++;
                data_splt2 = strtok(NULL, "=");
            }
        }
        datafile = fopen("/var/www/html/data.json", "r+");
        if (datafile != NULL)
        {
            time_t current_time;
            struct tm tm = *localtime(&current_time);
            current_time = time(NULL);
            tm = *localtime(&current_time);
            char time[1024];
            sprintf(time, "%d/%d/%d %02d:%02d", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min);
            char json[100] = "";
            sprintf(json, ",{\"name\":\"%s\",\"price\":\"%s\",\"time\":\"%s\"}]",  postedData[1], postedData[3], time);
            // delete ] at end of data.json file so a new json object can be added
            fseek(datafile, -1, SEEK_END);
            fputs(json, datafile);
            //printf(json);
        }
        fclose(datafile);
    }

    printf("</BODY>\n");
    printf("<script>");
    printf("window.addEventListener('load',()=>{");
    //redirect to the html page (client-side)
    printf("window.location.replace(\"http://piadosander.local\")})");
    printf("</script>");
    printf("</HTML>\n");

    return (EXIT_SUCCESS);
}